# identify-incomplete-xs-go-import-path

This program tries to identify go packages that have several Go import
paths but don't identify them in XS-Go-Import-Path.

It contains a hardcoded link to the results of a codesearch.debian.net
query.
