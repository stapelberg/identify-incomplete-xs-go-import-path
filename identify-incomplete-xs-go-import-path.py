#!/usr/bin/python3

import requests
from debian.deb822 import Deb822

#
# This program tries to identify go packages that have several Go import
# paths but don't identify it in XS-Go-Import-Path.
#
# It contains a hardcoded link to the results of a query on
# codesearch.debian.net.
#

def inspect_source_package(source_package):
        import_paths = get_xs_go_import_paths(source_package)

        if len(import_paths) == 0:
            print(
                "{source_package}: does not even use XS-Go-Import-Path".format(
                    source_package=source_package,
                )
            )
        elif len(import_paths) == 1:
            print(
                "{source_package}: only has one import path".format(
                    source_package=source_package,
                )
            )
        else:
            print(
                "{source_package}: looks fine!".format(
                    source_package=source_package,
                )
            )


def get_control_file(source_package):
    r = requests.get(
        "https://sources.debian.org/api/src/{source_package}/unstable/debian/control".format(
            source_package=source_package,
        )
    )
    control_file_info = r.json()
    control_file_url = control_file_info["raw_url"]
    control_file = requests.get("https://sources.debian.org" + control_file_url).text
    return control_file


def get_xs_go_import_paths(source_package):
        control_file = get_control_file(source_package)
        deb822 = Deb822(control_file)

        xs_go_import_path = deb822.get("XS-Go-Import-Path", None)

        if xs_go_import_path is None:
            return []

        import_paths = xs_go_import_path.split(",")

        return import_paths


def get_matching_source_packages():
    # Hardcoded result of this query on codesearch.debian.net:
    # "gocode path:debian/.*\.links package:golang.*"
    r = requests.get(
        "https://codesearch.debian.net/results/f9d797d3d9f1af9b/packages.json",
    )
    source_packages = r.json()["Packages"]
    return source_packages


def inspect_source_packages(source_packages):
    for source_package in source_packages:
        try:
            inspect_source_package(source_package)
        except Exception as ex:
            print(
                "{source_package}: Skipping... ({message})".format(
                    source_package=source_package,
                    message=str(ex),
                )
            )
            continue


def main():
    source_packages = get_matching_source_packages()
    inspect_source_packages(source_packages)


if __name__ == "__main__":
    main()
